import 'package:flutter/foundation.dart';

@immutable
abstract class CounterStates
{
  const CounterStates();
}

class InitialCounterState extends CounterStates {}

class SuccessCounterState extends CounterStates
{
  final String type;

  SuccessCounterState(this.type);
}